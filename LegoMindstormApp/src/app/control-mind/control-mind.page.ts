import { Component, OnInit } from '@angular/core';

// Modulo de rotacao de ecra
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';

//Modulo de apresentar menu
import { PopoverController } from '@ionic/angular';

// Componente de Loading Screen
import { PopoverComponent } from '../../app/components/popover/popover.component';

@Component({
  selector: 'app-control-mind',
  templateUrl: './control-mind.page.html',
  styleUrls: ['./control-mind.page.scss'],
})
export class ControlMindPage implements OnInit {

  constructor(private screenOrientation: ScreenOrientation, public popoverControler: PopoverController) { }

  async presentPopover(ev: any) {
    const popover = await this.popoverControler.create({
      component: PopoverComponent,
      event: ev,
      translucent: true
    });
    return await popover.present();
  }
  
  ngOnInit() {
    this.lockLandscape();
  }


//Rodar ecrã e prender Horizontal
lockLandscape(){
  this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.LANDSCAPE);
}
//Rodar ecra e prender Vertical
lockPortrait(){
  this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
}
//Desbloquear Rotação
unlock(){
  this.screenOrientation.unlock();
}
//Obter orientação do ecrã atual
getCurrentOrientation(){
  alert('Current Orientation is ' + this.screenOrientation.type);
}

ionViewDidLoad(){
  this.screenOrientation.onChange().subscribe(
    value => alert('Orientation Changed'),
    error => alert('Changed error: ' + error),
    () => alert('Done')
  )
}
 
}
