import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ControlMindPage } from './control-mind.page';

describe('ControlMindPage', () => {
  let component: ControlMindPage;
  let fixture: ComponentFixture<ControlMindPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ControlMindPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ControlMindPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
