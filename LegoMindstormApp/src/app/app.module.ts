import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';


import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
//Moudlo para a lista de items do popover
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

// Modulo de rotação de ecrã
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
// Modulo de Loading Screen
import { LoadingController } from '@ionic/angular';
// Modulo de Loading Screen
import { PopoverComponent } from './components/popover/popover.component';




@NgModule({
  declarations: [AppComponent, PopoverComponent],
  entryComponents: [PopoverComponent],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule,FormsModule],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    ScreenOrientation,LoadingController
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
