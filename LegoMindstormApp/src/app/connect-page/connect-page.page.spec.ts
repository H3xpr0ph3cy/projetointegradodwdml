import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConnectPagePage } from './connect-page.page';

describe('ConnectPagePage', () => {
  let component: ConnectPagePage;
  let fixture: ComponentFixture<ConnectPagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConnectPagePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConnectPagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
