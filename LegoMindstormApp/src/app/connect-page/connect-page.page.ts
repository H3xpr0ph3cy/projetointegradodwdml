import { Component, OnInit } from '@angular/core';
// Modulo de rotacao de ecra
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
// Modulo de rotacao de alerta
import { AlertController } from '@ionic/angular';

import { Router } from '@angular/router';
import { HomePage } from '../login-page/home.page';




@Component({
  selector: 'app-connect-page',
  templateUrl: './connect-page.page.html',
  styleUrls: ['./connect-page.page.scss'],
})
export class ConnectPagePage implements OnInit {

  constructor(private screenOrientation: ScreenOrientation, public alertController: AlertController, private router: Router) { }


  ngOnInit() {
    this.lockPortrait();
  }

  //Rodar ecra e prender Vertical
  lockPortrait() {
    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
  }

  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      header: 'Logout?',
      message: 'Are you sure you want to Logout?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Logout',
          handler: () => {
           this.router.navigate(['home']);
          }
        }
      ]
    });

    await alert.present();
  }

}
