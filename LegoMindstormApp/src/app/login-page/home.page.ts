import { Component } from '@angular/core';
import { LoadingController } from '@ionic/angular';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(private loadingCtrl: LoadingController) {

  }

  loginClicked() {
    //alert('Ai adoro');
    console.log("Clicked Login");
    this.loadingCtrl.create({
      message: "Logging in..."
    }).then((loading) => {
      loading.present();
    })
  }

  

}

