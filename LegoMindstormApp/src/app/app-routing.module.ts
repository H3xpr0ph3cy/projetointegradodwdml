import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./login-page/home.module').then( m => m.HomePageModule)},
  { path: 'connect-page', loadChildren: './connect-page/connect-page.module#ConnectPagePageModule' },
  { path: 'control-mind', loadChildren: './control-mind/control-mind.module#ControlMindPageModule' },

  


];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
